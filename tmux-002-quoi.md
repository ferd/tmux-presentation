TMUX, C'EST QUOI?
=================

Features
--------

- Une session de terminal, plusieurs tabs ou splits
- Reste vivant même après une déconnection
- Modes Emacs ou Vi
- Scriptable
- Configuration de base plus 'friendly' que screen

Commandes (quelques unes)
-------------------------

- ctrl + b  c       crée une fenêtre
- ctrl + b  0-9     choisir une fenêtre
- ctrl + b  n|p     next/prev fenêtre
- ctrl + b  d       déconnecter
- ctrl + b  :       command mode
- ctrl + b  [       selection mode
- ctrl + b  ]       coller (depuis sélection)
- ctrl + b  %       diviser la fenêtre verticalement
- ctrl + b  "       diviser la fenêtre horizontalement
- ctrl + b  z       zoom sur un 'pane'
