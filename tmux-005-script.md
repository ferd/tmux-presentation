SCRIPTING
=========

Quelques commandes
------------------
- tmux has-session -t $SESSION
- tmux new-session -s $SESSION -d (detached)
- tmux new-window -c $CODE -t $SESSION
- tmux send-keys -t $SESSION: 'Str' [C-m]
- tmux send-keys -t $SESSION:<window> 'Str' [C-m]
- tmux send-keys -t $SESSION:<window>.<pane> 'Str' [C-m]
- tmux split-window -h -c $CODE -t $SESSON
- tmux select-window -t $SESSION:<window>
- tmux att -t $SESSION
- tmux kill-session -t $SESSION
- if [ $? != 0 ] then ... fi

Demo
----
Script de cette présentation
