CONFIGURATION
=============

gestion du terminal
-------------------

    set-option -g default-shell $SHELL
    set-window-option -g utf8 on

raccourcis clavier
------------------

    bind-key c new-window -c '#{pane_current_path}'
    bind-key '"' split-window -c '#{pane_current_path}'
    bind-key % split-window -h -c '#{pane_current_path}'

{PRO MODE}
----------

    set -g status-right '📅  #(gcalcli --client_id $GCAL_CLIENT_ID --client_secret $GCAL_SECRET --nostarted --calendar "fred@heroku.com" agenda --nocolor --military | cut -d " " -f 4- | sed "s/^ *//g" | sed "s/  / /g" | cut -c1-34 | awk "NR==2")'
