VARIA
=====

Gestion de connections:

- tmux list-clients
- tmux kill-clients

Autres features non couvertes:

- sessions partagées
- sessions sur comptes différents

livre
-----

    TMUX: productive mouse-free development
        par Brian P. Hogan

    URL: https://pragprog.com/book/bhtmux/tmux
    Copie gratuite: http://lmgtfy.com/?q=tmux+pdf
